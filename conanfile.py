import os
from os import path

try:
    import stat
except:
    from os import stat

from conans import ConanFile, tools
from conans.tools import download, check_sha256, pythonpath

class glibConanFile(ConanFile):
    name        = "glib"
    version     = "2.48.2"
    description = "general-purpose utility library, which provides many types, macros, a mainloop abstraction, and other utilities."
    license     = "LGPL"
    url         = "https://gitlab.com/no-face/glib-conan"  #recipe repo url
    glib_urls   = {
        "project" : "https://wiki.gnome.org/Projects/GLib",
        "repo"    : "https://github.com/GNOME/glib",
        "docs"    : "https://developer.gnome.org/glib/"
    }

    settings    = "os", "compiler", "arch"
    options = {
        "shared"  : [True, False],
        "use_pic" : [True, False],

        "pcre"      : ["internal", "system"],
        "libiconv"  : ["no", "gnu", "native"],
        "libintl"   : ["gnu", "system"],

        "use_gio"     : [True, False],
        "use_gobject" : [True, False],
        "use_gmodule" : [True, False],
    }
    default_options = (
        "shared=True",
        "use_pic=True",
        "pcre=internal",
        "libiconv=gnu",
        "libintl=system",

        "use_gio=True",
        "use_gobject=True",
        "use_gmodule=True"
    )

    requires    = (
        "zlib/1.2.11@conan/stable",
        "libffi/3.2.1@noface/stable"
    )

    build_requires    = (
        "untarxz/0.0.1@noface/testing",
        "AutotoolsHelper/0.1.0@noface/stable"
    )

    exports_sources = "patches/*"

    BASE_URL_DOWNLOAD       = "https://download.gnome.org/sources/" + name
    FILE_URL                = "{}/{}/{}-{}.tar.xz".format(BASE_URL_DOWNLOAD, "2.48", name, version)
    EXTRACTED_FOLDER_NAME   = "{}-{}".format(name, version)
    FILE_SHA256             = 'f25e751589cb1a58826eac24fbd4186cda4518af772806b666a3f91f66e6d3f4'

    def config_options(self):
        if self.settings.os == "Android":
            self.options.libintl="gnu"

    def configure(self):
        # pure-c library
        del self.settings.compiler.libcxx

        if self.options.shared==True or self.options.use_pic == True:
            self.options.use_pic                    = True
            self.options["libffi"].use_pic          = True
            self.options["gettext-runtime"].use_pic = True

    def requirements(self):
        if self.options.libintl == "gnu":
            self.requires("gettext-runtime/0.19.8@noface/stable")

        if self.options.libiconv == "gnu":
            self.requires("libiconv/1.14@noface/stable")

    def source(self):
        zip_name = self.name + ".tar.xz"
        download(self.FILE_URL, zip_name)
        check_sha256(zip_name, self.FILE_SHA256)

        with pythonpath(self):
            from untarxz import untarxz
            untarxz(self, zip_name)

        os.remove(zip_name)

    def build(self):
        with tools.environment_append(self.make_build_env()):
            self.configure_and_make()

    def package(self):
        #Files installed in build step
        pass

    def package_info(self):
        includes = [
            path.join("include", "glib-2.0"),
            path.join("lib", "glib-2.0", "include") # adds glibconfig.h
        ]

        unix_includes = path.join(self.package_folder, "include", "gio-unix-2.0")
        if path.exists(unix_includes):
            includes.append(unix_includes)

        self.cpp_info.includedirs   = includes
        self.cpp_info.bindirs       = ["bin"]
        self.cpp_info.libs          = self.make_libs()

    def build_id(self):
        # These options does not change the build
        self.info_build.options.use_gio = ''
        self.info_build.options.use_gobject = ''
        self.info_build.options.use_gmodule = ''

    ##################################################################################################
    
    def make_build_env(self):
        ffi_deps = self.deps_cpp_info["libffi"]
        LIBFFI_CFLAGS = " -I".join([""] + ffi_deps.include_paths)
        LIBFFI_LIBS   = " -L".join([""] + ffi_deps.lib_paths)
        LIBFFI_LIBS  += " -l".join([""] + ffi_deps.libs)

        return {'LIBFFI_CFLAGS' : LIBFFI_CFLAGS, 'LIBFFI_LIBS' : LIBFFI_LIBS}

    def make_libs(self):
        libs = []
        if self.options.use_gio:
            libs.append("gio-2.0")
        if self.options.use_gobject:
            libs.append("gobject-2.0")
        if self.options.use_gmodule:
            libs.append("gmodule-2.0")

        libs.append("glib-2.0")

        return libs

    def configure_and_make(self):
        with tools.chdir(self.EXTRACTED_FOLDER_NAME), pythonpath(self):
            from autotools_helper import Autotools

            autot = Autotools(self,
                shared      = self.options.shared)

            autot.fpic = self.options.use_pic
            autot.with_feature("pcre", self.options.pcre)
            autot.with_feature("libiconv", self.options.libiconv)

            self.setup_cache_file(autot)

            if self.settings.os == "Android":
                autot.defines.append("HAVE_GETPWUID_R=0")

            autot.configure(**self.configure_args())
            autot.build()
            autot.install()

    def configure_args(self):
        s = self.settings

        default_args = {}

        if not tools.cross_building(s) or s.compiler != "gcc":
            return default_args

        if s.os != self.platform_os:
            return default_args

        if s.arch != "x86" and self.platform_arch == "x86_64":
            # Prevent traditional cross-compile when building
            # from x64 to x86 in gcc, because this requires 'gcc-multilib g++-multilib'
            # Setting values to False, prevents then to be set in configure.
            return {'build' : False, 'host' : False, 'target' : False}

        return default_args


    @property
    def platform_os(self):
        import platform

        return self.settings.get_safe("os_build") or \
            {"Darwin": "Macos"}.get(platform.system(), platform.system())

    @property
    def platform_arch(self):
        return self.settings.get_safe("arch_build") or tools.detected_architecture()

    def setup_cache_file(self, autot):
        '''Glib requires a 'cache' file to cross compile.'''

        if not tools.cross_building(self.settings):
            return

        cache_file = None

        if self.settings.os == "Android":
            cache_file = path.join(self.source_folder, "patches", "glib-android.cache")
        elif self.settings.os == "Linux" and self.settings.arch == "x86":
            # File may be valid to other configurations (or not)
            cache_file = path.join(self.source_folder, "patches", "glib-x86-linux.cache")
        else:
            return #No cache file

        # Change permissions over cache file to avoid configure changing file
        os.chmod(cache_file, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)

        autot.add_arg("--cache-file={}".format(cache_file))

