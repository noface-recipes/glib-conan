#include <iostream>

#include <glib.h>

bool checkGlib();

int main(void){

    std::cout << "*************** Testing glib ******************" << std::endl;

    std::cout << (checkGlib() ? "Ok" : "False") << std::endl;

    std::cout << "***********************************************" << std::endl;   
    
    return 0;
}

// Simple code to check if library was compiled correctly
bool checkGlib() {

    GList* list = g_list_append(NULL, (void *)"Hello");
    g_list_append(list, (void *)"World!");

    GList * item= list;
    while(item != NULL){
        std::cout << (const char *) item->data << " ";
        item = item->next;
    }
    std::cout << std::endl;

    return true;
}

