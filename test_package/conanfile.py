#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
from os import path

class TestPackage(ConanFile):
    settings = "os", "compiler", "arch"

    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.1.0@noface/testing"
    )

    generators = "Waf"
    exports = "wscript"

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")    # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        self.build_path = path.abspath("build")

        self.run(
            "waf configure build -v -o %s" % (self.build_path),
            cwd=self.source_folder)

    def test(self):
        exec_path = path.join(self.build_path, 'example')
        self.output.info("running test: " + exec_path)
        self.run(exec_path)
