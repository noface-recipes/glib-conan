# glib-conan

[Conan.io](https://www.conan.io/) package for [libglib](https://wiki.gnome.org/Projects/GLib).

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
glib [reference manual](https://developer.gnome.org/glib/) to instruction in how to use the library.


## About libglib

    GLib is a general-purpose utility library, which provides many useful data types, macros,
    type conversions, string utilities, file utilities, a mainloop abstraction, and so on.
    It works on many UNIX-like platforms, as well as Windows and OS X.

Reference: [glib overview](https://developer.gnome.org/glib/2.50/glib.html).


## License

This conan package is distributed under the [unlicense](http://unlicense.org/) terms (see LICENSE.md).

GLib is released under the GNU Library General Public License (GNU LGPL) version 2.
The license file and source code are available at [github](https://github.com/GNOME/glib)
or [gnome git repository](https://git.gnome.org//browse/glib/).

See the COPYING file [here](https://git.gnome.org/browse/glib/tree/COPYING) or [here](https://github.com/GNOME/glib/blob/master/COPYING).


## Limitations

This package does not support all glib configure options or OSes.
So contribute if you can.
